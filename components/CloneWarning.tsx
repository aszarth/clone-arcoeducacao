"use client";

import Button from "./Button";
import Icon from "./Icon";

export default function CloneWarning({
  originalUrl,
  gitUrl,
  readWarning,
  setReadWarning,
}: {
  originalUrl: string;
  gitUrl: string;
  readWarning: boolean;
  setReadWarning: Function;
}) {
  return (
    <>
      {!readWarning && (
        <>
          <div
            className="greyBG"
            style={{
              backgroundColor: "rgba(0, 0, 0, 0.3)",
              width: "100vw",
              height: "100vh",
              zIndex: 20,
              position: "fixed",
            }}
            onClick={() => {
              setReadWarning(true);
            }}
          ></div>
          <div
            className="whiteContent"
            style={{
              flex: 1,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              zIndex: 30,
              position: "fixed",
              // align center position absolute
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
          >
            <div
              style={{
                backgroundColor: "#FFF",
                width: 300,
                height: 250,
                padding: 10,
                borderRadius: 10,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                textAlign: "center",
              }}
            >
              <div
                style={{
                  height: 260,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                <span>
                  Este site é um clone, não é o site original, acesse o site
                  original em:{" "}
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon name="internet" width={16} height={16} color="#000" />
                  <strong
                    style={{ fontSize: 12, cursor: "pointer" }}
                    onClick={() => {
                      window.open(originalUrl, "_blank");
                    }}
                  >
                    {originalUrl}
                  </strong>
                </div>
                <span style={{ marginTop: 10 }}>
                  Código aberto do clone do site em:{" "}
                </span>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon name="git" width={16} height={16} color="#000" />
                  <strong
                    style={{ fontSize: 12, cursor: "pointer" }}
                    onClick={() => {
                      window.open(gitUrl, "_blank");
                    }}
                  >
                    {gitUrl}
                  </strong>
                </div>
              </div>
              <div style={{ height: 80 }}>
                <Button
                  width={250}
                  height={40}
                  style={{ marginTop: 10 }}
                  onClick={() => {
                    setReadWarning(true);
                  }}
                  shadowButton={false}
                  shadowLeft={14}
                  shadowBottom={35}
                  borderRadius={25}
                >
                  Entendi...
                </Button>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}
