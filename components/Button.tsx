"use client";
import colors from "@/libs/colors";
import { useState } from "react";

export default function Button({
  children,
  width,
  height,
  style,
  onClick,
  shadowButton,
  shadowLeft,
  shadowBottom,
  borderRadius,
}: {
  children: string;
  width: number;
  height: number;
  style?: object;
  onClick: Function;
  shadowButton: boolean;
  shadowLeft: number;
  shadowBottom: number;
  borderRadius?: number;
}) {
  const [isMouseHover, setIsMouseHover] = useState(false);
  return (
    <>
      <button
        className="className"
        style={{
          position: "relative",
          width: width,
          height: height,
          backgroundColor: isMouseHover ? colors.primary_two : "#9295F8",
          color: colors.white,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          textAlign: "center",
          cursor: "pointer",
          fontSize: 14,
          borderRadius: borderRadius ? borderRadius : "0px 15px 15px 15px",
          border: "4px solid #FFFFFF",
          zIndex: 15,
          ...style,
        }}
        onMouseEnter={() => {
          setIsMouseHover(true);
        }}
        onMouseLeave={() => {
          setIsMouseHover(false);
        }}
        onClick={() => onClick()}
      >
        {children}
      </button>
      {shadowButton && (
        <div
          className="btnBg"
          style={{
            position: "relative",
            left: shadowLeft,
            bottom: shadowBottom,
            width: width - 10,
            height: height,
            background: colors.primary_two,
            borderRadius: "0px 15px 15px",
            zIndex: 10,
          }}
        ></div>
      )}
    </>
  );
}
