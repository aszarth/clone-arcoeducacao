import colors from "@/libs/colors";
import Icon from "./Icon";

export default function Footer({ isMobile }: { isMobile: boolean }) {
  return (
    <footer
      style={{
        height: 320,
        width: "100vw",
        backgroundColor: "#011938",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <div
        style={{
          width: "80vw",
          height: !isMobile ? 300 : "100vh",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        {!isMobile && (
          <img
            src="/logo.png"
            width={193}
            height={41}
            alt="Imagem do Logotipo do Rodapé"
          />
        )}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: !isMobile ? "space-between" : "center",
            alignItems: !isMobile ? undefined : "center",
            height: !isMobile ? undefined : 200,
          }}
        >
          <div />
          <Icon
            name="linkedin"
            width={28}
            height={28}
            color={colors.white}
            onClick={() => alert("go to linkedin")}
          />
        </div>
      </div>
    </footer>
  );
}
