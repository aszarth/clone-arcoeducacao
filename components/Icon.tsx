"use client";
import SvgGithub from "./svgs/github.svg";
import SvgInternet from "./svgs/internet-explorer.svg";
import SvgBars from "./svgs/bars.svg";
import SvgLinkedin from "./svgs/linkedin.svg";
import SvgPlus from "./svgs/plus.svg";
import SvgAngleLeft from "./svgs/angle-left.svg";
import SvgAngleRight from "./svgs/angle-right.svg";

export default function Icon({
  name,
  width,
  height,
  color,
  style,
  onClick,
  className,
}: {
  name:
    | "internet"
    | "git"
    | "bars"
    | "linkedin"
    | "plus"
    | "angle-left"
    | "angle-right";
  width: number;
  height: number;
  color: string;
  style?: object;
  onClick?: Function;
  className?: string;
}) {
  return (
    <div
      style={{ width: width, height: height, ...style }}
      onClick={() => {
        if (onClick) onClick();
      }}
      className={className}
    >
      {name == "internet" && (
        <SvgInternet fill={color} width={width} height={height} />
      )}
      {name == "git" && (
        <SvgGithub fill={color} width={width} height={height} />
      )}
      {name == "bars" && <SvgBars fill={color} width={width} height={height} />}
      {name == "linkedin" && (
        <SvgLinkedin fill={color} width={width} height={height} />
      )}
      {name == "plus" && <SvgPlus fill={color} width={width} height={height} />}
      {name == "angle-right" && (
        <SvgAngleRight fill={color} width={width} height={height} />
      )}
      {name == "angle-left" && (
        <SvgAngleLeft fill={color} width={width} height={height} />
      )}
    </div>
  );
}
