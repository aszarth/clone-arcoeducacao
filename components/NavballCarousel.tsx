export default function NavballCarousel({
  isSelected,
  onClick,
}: {
  isSelected: boolean;
  onClick: Function;
}) {
  return (
    <li
      onClick={() => onClick()}
      style={{
        borderRadius: "50%",
        height: 20,
        width: 20,
        background: isSelected ? "#000" : "#fff",
        border: "1px solid #80D0DB",
        marginLeft: 10,
        cursor: "pointer",
      }}
    ></li>
  );
}
