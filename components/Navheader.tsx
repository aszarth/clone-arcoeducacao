import colors from "@/libs/colors";
import Icon from "./Icon";
import { useState } from "react";
import { useGlobalRefs } from "@/contexts/GlobalRefsContext";

export default function Naveheader({ isMobile }: { isMobile: boolean }) {
  const { jobsGlobalRef, aboutusGlobalRef, ourwayGlobalRef, talentsGlobalRef } =
    useGlobalRefs();
  function ScrollToRef(type: "jobs" | "aboutus" | "ourway" | "talent") {
    switch (type) {
      case "jobs": {
        if (jobsGlobalRef && jobsGlobalRef.current) {
          jobsGlobalRef.current.scrollIntoView({ behavior: "smooth" });
        }
        break;
      }
      case "aboutus": {
        if (aboutusGlobalRef && aboutusGlobalRef.current) {
          aboutusGlobalRef.current.scrollIntoView({ behavior: "smooth" });
        }
        break;
      }
      case "ourway": {
        if (ourwayGlobalRef && ourwayGlobalRef.current) {
          ourwayGlobalRef.current.scrollIntoView({ behavior: "smooth" });
        }
        break;
      }
      case "talent": {
        if (talentsGlobalRef && talentsGlobalRef.current) {
          talentsGlobalRef.current.scrollIntoView({ behavior: "smooth" });
        }
        break;
      }
    }
  }
  return (
    <header>
      {isMobile ? (
        <NavheaderMOBILE ScrollToRef={ScrollToRef} />
      ) : (
        <NavheaderPC ScrollToRef={ScrollToRef} />
      )}
    </header>
  );
}

function NavheaderMOBILE({ ScrollToRef }: { ScrollToRef: Function }) {
  const [isNavItemsOpen, setIsNavItemsOpen] = useState(false);
  return (
    <div
      style={{
        height: isNavItemsOpen ? 200 : 60,
        width: "100vw",
        backgroundColor: colors.primary,
        display: "flex",
        flexDirection: "column",
        paddingRight: 20,
        paddingLeft: 20,
        transition: "height 0.25s ease-in-out",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginTop: 10,
        }}
      >
        <img
          src="/logo.png"
          alt="Imagem do Logotipo do Cabeçalho"
          width={193}
          height={41}
        />
        <div
          style={{
            borderColor: "rgba(255,255,255,.1)",
            borderStyle: "solid",
            borderWidth: 1,
            width: 56,
            height: 40,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
          onClick={() => setIsNavItemsOpen(!isNavItemsOpen)}
        >
          <Icon
            name="bars"
            width={30}
            height={30}
            color="rgba(255,255,255,.5)"
          />
        </div>
      </div>
      <nav>
        <h2 className="visually-hidden">Navegação Principal</h2>
        <ul
          style={{
            display: isNavItemsOpen ? "flex" : "none",
            flexDirection: "column",
            alignItems: "center",
            height: isNavItemsOpen ? 150 : 0,
          }}
        >
          <ActionButton
            onClick={() => {
              ScrollToRef("jobs");
            }}
          />
          <NavItem
            style={{ marginTop: 15 }}
            onClick={() => {
              ScrollToRef("aboutus");
            }}
          >
            Conheça a ArcoTech
          </NavItem>
          <NavItem
            style={{ marginTop: 15 }}
            onClick={() => {
              ScrollToRef("ourway");
            }}
          >
            Nosso jeito
          </NavItem>
          <NavItem
            style={{ marginTop: 15 }}
            onClick={() => {
              ScrollToRef("talent");
            }}
          >
            Banco de Talentos
          </NavItem>
        </ul>
      </nav>
    </div>
  );
}

function NavheaderPC({ ScrollToRef }: { ScrollToRef: Function }) {
  return (
    <nav
      style={{
        height: 67,
        backgroundColor: colors.primary,
        width: "100vw",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
      }}
    >
      <h2 className="visually-hidden">Navegação Principal</h2>
      <img
        src="/logo.png"
        alt="Imagem do Logotipo do Cabeçalho"
        width={193}
        height={41}
      />
      <ul
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <ActionButton
          onClick={() => {
            ScrollToRef("jobs");
          }}
        />
        <Separator />
        <NavItem
          onClick={() => {
            ScrollToRef("aboutus");
          }}
        >
          Conheça a ArcoTech
        </NavItem>
        <Separator />
        <NavItem
          onClick={() => {
            ScrollToRef("ourway");
          }}
        >
          Nosso jeito
        </NavItem>
        <Separator />
        <NavItem
          onClick={() => {
            ScrollToRef("talent");
          }}
        >
          Banco de Talentos
        </NavItem>
      </ul>
    </nav>
  );
}

function NavItem({
  children,
  onClick,
  style,
}: {
  children: string;
  onClick: Function;
  style?: object;
}) {
  return (
    <li style={{ ...style }}>
      <a
        style={{
          color: colors.white,
          fontSize: 16,
          cursor: "pointer",
        }}
        onClick={() => onClick()}
      >
        {children}
      </a>
    </li>
  );
}

function Separator() {
  return (
    <li>
      <div
        style={{
          backgroundColor: colors.white,
          height: 32,
          width: 1,
          marginLeft: 25,
          marginRight: 25,
        }}
      ></div>
    </li>
  );
}

function ActionButton({ onClick }: { onClick: Function }) {
  return (
    <li style={{ cursor: "pointer" }}>
      <button
        style={{
          backgroundColor: "#E8442E",
          width: 190,
          height: 35,
          borderRadius: 20,
          color: colors.white,
          fontSize: 16,
          fontWeight: "bold",
          cursor: "pointer",
        }}
        onClick={() => onClick()}
      >
        VAGAS ABERTAS
      </button>
    </li>
  );
}
