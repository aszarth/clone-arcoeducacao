"use client";
import AboutUs from "@/app/_components/AboutUs";
import Depositions from "@/app/_components/Depositions";
import HeroCard from "@/app/_components/HeroCard";
import OpenJobs from "@/app/_components/OpenJobs";
import OurValues from "@/app/_components/OurValues";
import CampanyNumbers from "@/app/_components/CampanyNumbers";
import Photos from "@/app/_components/Photos";
import TalentPool from "@/app/_components/TalentPool";
import CheckIsMobile from "@/libs/checkIsMobile";
import { useEffect, useState } from "react";

export default function Home() {
  // mobile detect
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);
  return (
    <main
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <HeroCard />
      <AboutUs isMobile={isMobile} /> {/* nav2 */}
      <OpenJobs isMobile={isMobile} /> {/* nav1 */}
      <TalentPool isMobile={isMobile} /> {/* nav4 */}
      <CampanyNumbers isMobile={isMobile} /> {/* nav3 */}
      <OurValues isMobile={isMobile} />
      <Photos isMobile={isMobile} />
      <Depositions isMobile={isMobile} />
    </main>
  );
}
