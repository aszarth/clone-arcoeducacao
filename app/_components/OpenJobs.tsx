import { useGlobalRefs } from "@/contexts/GlobalRefsContext";
import colors from "@/libs/colors";
import { useVisibility } from "@/libs/useVisibility";

export default function OpenJobs({ isMobile }: { isMobile: boolean }) {
  const { jobsGlobalRef } = useGlobalRefs();
  const hasContainerShowed = useVisibility(jobsGlobalRef);
  //
  const orange = "#E8442E";
  return (
    <section
      ref={jobsGlobalRef}
      style={{
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
        width: "100vw",
        height: 400,
        backgroundColor: colors.primary,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <h2
        style={{
          color: colors.white,
          fontSize: isMobile ? 32 : 64,
          fontStyle: "normal",
          paddingBottom: 30,
          paddingTop: 30,
          fontWeight: 500,
        }}
      >
        Conheça nossas <span style={{ color: orange }}>vagas </span>
      </h2>
      <div
        style={{
          width: "100vw",
          height: 200,
          backgroundColor: orange,
        }}
      ></div>
    </section>
  );
}
