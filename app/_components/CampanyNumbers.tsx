import colors from "@/libs/colors";
import Icon from "../../components/Icon";
import { useVisibility } from "@/libs/useVisibility";
import { useGlobalRefs } from "@/contexts/GlobalRefsContext";

export default function CampanyNumbers({ isMobile }: { isMobile: boolean }) {
  const { ourwayGlobalRef } = useGlobalRefs();
  const hasContainerShowed = useVisibility(ourwayGlobalRef);
  return (
    <section
      ref={ourwayGlobalRef}
      style={{
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
        width: "100vw",
        height: 780,
        backgroundImage: `url('https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-192-1.jpg')`,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
        padding: 30,
      }}
    >
      {!isMobile && <NumbersHeader />}
      <NumberContent isMobile={isMobile} />
      {!isMobile && <NumberFooter />}
    </section>
  );
}

function NumbersHeader() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        height: 200,
        marginTop: 50,
      }}
    >
      <Icon name="plus" width={86} height={86} color={colors.white} />
      <h2
        style={{
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 64,
          lineHeight: 80,
          color: colors.white,
        }}
      >
        Números que dão orgulho
      </h2>
      <div />
    </div>
  );
}

function NumberContent({ isMobile }: { isMobile: boolean }) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        justifyContent: "space-around",
        alignItems: "center",
        width: "100%",
        height: isMobile ? "100%" : 200,
        paddingRight: 30,
        paddingLeft: 30,
        fontSize: 36,
        color: colors.white,
        textAlign: "center",
      }}
    >
      <p>
        <strong>3 milhões</strong>
        <br /> de alunos
      </p>
      <p>
        <strong>8 mil</strong>
        <br /> escolas
      </p>
      <p>
        <strong>Mais de 150</strong>
        <br /> colaboradores
      </p>
    </div>
  );
}

function NumberFooter() {
  return (
    <div style={{ display: "flex", justifyContent: "flex-end" }}>
      <img
        src="https://arcoeducacao.com.br/wp-content/uploads/2022/02/vector_arcotech-2.png"
        alt="Icone de Mais/Adicionar"
      />
    </div>
  );
}
