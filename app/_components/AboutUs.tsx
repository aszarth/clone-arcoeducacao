import { useGlobalRefs } from "@/contexts/GlobalRefsContext";
import colors from "@/libs/colors";
import { useVisibility } from "@/libs/useVisibility";

export default function AboutUs({ isMobile }: { isMobile: boolean }) {
  const { aboutusGlobalRef } = useGlobalRefs();
  const hasContainerShowed = useVisibility(aboutusGlobalRef);
  return (
    <section
      ref={aboutusGlobalRef}
      style={{
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        justifyContent: !isMobile ? "space-between" : undefined,
        maxWidth: !isMobile ? 1140 : "100%",
        paddingLeft: 15,
        paddingRight: 15,
      }}
    >
      <img
        src="https://arcoeducacao.com.br/wp-content/uploads/2022/01/Group-9.png"
        alt="Foto de Pessoas do Sobre Nós"
      />
      <div style={{ width: !isMobile ? "40%" : "100%" }}>
        <h2
          style={{
            color: colors.secondary,
            fontSize: !isMobile ? 64 : 32,
            fontStyle: "normal",
            fontWeight: 500,
            marginTop: 15,
          }}
        >
          Conheça a ArcoTech
        </h2>
        <div style={{ color: colors.black, fontSize: 18 }}>
          <p style={{ marginTop: 15 }}>
            Na ArcoTech, reunimos talentos que têm paixão por tecnologia e
            educação.{" "}
          </p>
          <p style={{ marginTop: 30 }}>
            Sabemos que, juntos, somos capazes de unir as duas forças para
            elevar a experiência de ensino e aprendizagem a um novo patamar.
          </p>
          <p style={{ marginTop: 30 }}>
            Queremos ir além da inovação, nosso propósito é potencializar o
            impacto da educação nas pessoas, é o que nos move.
          </p>
        </div>
      </div>
    </section>
  );
}
