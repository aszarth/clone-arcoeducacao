import colors from "@/libs/colors";
import { useRef } from "react";
import { useVisibility } from "@/libs/useVisibility";

export default function OurValues({ isMobile }: { isMobile: boolean }) {
  const containerRef = useRef(null);
  const hasContainerShowed = useVisibility(containerRef);
  return (
    <section
      ref={containerRef}
      style={{
        height: isMobile ? 2600 : 900,
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
      }}
    >
      <h2
        style={{
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 64,
          textAlign: "center",
          color: "#011938",
        }}
      >
        Nossos valores
      </h2>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          maxWidth: 1200,
          marginTop: 10,
        }}
      >
        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-76-2.png"
          title="Ética e Respeito"
          content="Falar a verdade e agir com respeito é
        essencial na construção de uma
        empresa consistente e duradoura.
        Estamos aqui para fazer o correto, sem
        atalhos"
        />
        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-77-2.png"
          title="Educação é propósito de vida"
          content="Temos a Educação como paixão,
          vocação e sonho. Acreditamos que ela
          transforma as pessoas e muda o mundo."
        />
        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-126.png"
          title="Humildade e pé no chão"
          content="A vitória de hoje em nada garante o
          sucesso de amanhã. Para criarmos uma
          empresa sustentável, buscamos, com
          humildade, corrigir nossos erros e
          melhorar sempre. Um passo de cada vez."
        />

        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-80-2.png"
          title="O valor está nas pessoas"
          content="Somente com pessoas excelentes que
pensam e agem como donas, trabalham
juntas e são recompensadas pelas
entregas, se constrói uma companhia
perene."
        />
        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-81-2.png"
          title="Compromisso com os nossos clientes"
          content="
Respeitamos nossos clientes e nossos
obstinados em entregar soluções de
excelência. Buscamos execução
eficiente com muita disciplina."
        />
        <ValueCard
          iconImgSrc="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-131.png"
          title="Evoluir sempre
"
          content="Para o sucesso no longo prazo,
precisamos sonhar grande, evoluir
sempre e, por vezes, abrir mão de
resultados no curto prazo. Por isso,
buscamos inovar e investir cada vez
mais."
        />
      </div>
    </section>
  );
}

function ValueCard({
  iconImgSrc,
  title,
  content,
}: {
  iconImgSrc: string;
  title: string;
  content: string;
}) {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        width: 400,
        height: 400,
        textAlign: "center",
      }}
    >
      <img
        src={iconImgSrc}
        alt={`Imagem Valor: ${title}`}
        width={117}
        height={117}
      />
      <h3
        style={{
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: 20,
          color: colors.primary_two,
          marginTop: 10,
        }}
      >
        {title}
      </h3>
      <p
        style={{
          fontStyle: "normal",
          fontWeight: "normal",
          fontSize: 16,
          color: colors.black,
          marginTop: 10,
          paddingRight: 60,
          paddingLeft: 60,
        }}
      >
        {content}
      </p>
    </div>
  );
}
