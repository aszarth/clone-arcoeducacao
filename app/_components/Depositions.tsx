import NavballCarousel from "@/components/NavballCarousel";
import { useVisibility } from "@/libs/useVisibility";
import { useRef, useState } from "react";

export default function Depositions({ isMobile }: { isMobile: boolean }) {
  const containerRef = useRef(null);
  const hasContainerShowed = useVisibility(containerRef);
  const [depositionSelected, setDepositionSelected] = useState(0);
  return (
    <section
      ref={containerRef}
      style={{
        height: isMobile ? 500 : 700,
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
      }}
    >
      <h3
        style={{
          fontSize: isMobile ? 32 : 64,
          fontWeight: 500,
          fontStyle: "normal",
          color: "#011938",
          textAlign: "center",
          paddingBottom: 50,
        }}
      >
        Depoimentos
      </h3>
      {depositionSelected <= 0 && (
        <DepositionCard
          isMobile={isMobile}
          photo="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-26.png"
          text="Tenho aprendido tanto com o time e com os professores que, cada vez mais, percebo o impacto da Studos em minha carreira!"
          name="Luiz Filipi Schveitzer - Time Editorial"
        />
      )}
      {depositionSelected == 1 && (
        <DepositionCard
          isMobile={isMobile}
          photo="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-181.png"
          text="Fazer parte de UX significa focar nossos esforços em compreender a realidade das escolas e de realmente estar próximo."
          name="Marianne Braum - Líder de design de produto"
        />
      )}
      <div
        style={{
          height: "2px",
          width: "100%",
          backgroundColor: "#447FC0",
          marginTop: 25,
        }}
      ></div>
      <ol
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          paddingBottom: 20,
          marginTop: 25,
        }}
      >
        <NavballCarousel
          isSelected={Boolean(depositionSelected === 0)}
          onClick={() => {
            setDepositionSelected(0);
          }}
        />
        <NavballCarousel
          isSelected={Boolean(depositionSelected === 1)}
          onClick={() => {
            setDepositionSelected(1);
          }}
        />
        <NavballCarousel
          isSelected={false}
          onClick={() => {
            setDepositionSelected(0);
          }}
        />
      </ol>
    </section>
  );
}

function DepositionCard({
  isMobile,
  photo,
  text,
  name,
}: {
  isMobile: boolean;
  photo: string;
  text: string;
  name: string;
}) {
  return (
    <div
      style={{
        width: "80vw",
        display: "flex",
        flexDirection: "row",
        padding: !isMobile ? 50 : undefined,
      }}
    >
      {!isMobile && (
        <img
          src={photo}
          width={232}
          height={232}
          style={{ borderRadius: "100%" }}
          alt="Foto de um Usuário que deu depoimento"
        />
      )}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <p
          style={{
            fontStyle: "italic",
            fontWeight: 300,
            fontSize: !isMobile ? 38 : 24,
            lineHeight: !isMobile ? "40px" : undefined,
            color: "#777BF7",
            padding: !isMobile ? 25 : undefined,
          }}
        >
          “{text}”
        </p>
        <p
          style={{
            fontStyle: "italic",
            fontWeight: "bold",
            fontSize: !isMobile ? 22 : 15,
            lineHeight: !isMobile ? "21px" : undefined,
            marginTop: isMobile ? 10 : 0,
            color: "#D98839",
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          {name}
        </p>
      </div>
    </div>
  );
}
