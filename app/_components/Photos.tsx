import colors from "@/libs/colors";
import Icon from "../../components/Icon";
import { useRef, useState } from "react";
import NavballCarousel from "@/components/NavballCarousel";
import { useVisibility } from "@/libs/useVisibility";

export default function Photos({ isMobile }: { isMobile: boolean }) {
  const containerRef = useRef(null);
  const hasContainerShowed = useVisibility(containerRef);
  return (
    <section
      ref={containerRef}
      style={{
        height: isMobile ? 500 : 900,
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
      }}
    >
      <h3
        style={{
          fontSize: isMobile ? 32 : 64,
          fontWeight: 500,
          fontStyle: "normal",
          color: "#011938",
          textAlign: "center",
          paddingBottom: 50,
        }}
      >
        Fotos da ArcoTech
      </h3>
      <PhotoCarousel
        imgs={[
          "https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-88.png",
          "https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-88.png",
          "https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-88.png",
        ]}
        isMobile={isMobile}
      />
    </section>
  );
}

function PhotoCarousel({
  imgs,
  isMobile,
}: {
  imgs: string[];
  isMobile: boolean;
}) {
  const [imgSelected, setImgSelected] = useState(0);
  return (
    <div
      style={{
        backgroundImage: `url("${imgs[imgSelected]}")`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: !isMobile ? "80vw" : 380,
        height: !isMobile ? 700 : 250,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <div />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Icon
          name="angle-left"
          width={48}
          height={48}
          color={colors.primary_two}
          onClick={() => {
            if (imgSelected > 0) {
              setImgSelected(imgSelected - 1);
            } else {
              setImgSelected(2);
            }
          }}
          style={{
            cursor: "pointer",
            position: isMobile ? "absolute" : undefined,
            left: isMobile ? -10 : undefined,
          }}
        />
        <Icon
          name="angle-right"
          width={48}
          height={48}
          color={colors.primary_two}
          onClick={() => {
            if (imgSelected < 2) {
              setImgSelected(imgSelected + 1);
            } else {
              setImgSelected(0);
            }
          }}
          style={{
            cursor: "pointer",
            position: isMobile ? "absolute" : undefined,
            right: isMobile ? -10 : undefined,
          }}
        />
      </div>
      <ol
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          paddingBottom: 20,
        }}
      >
        <NavballCarousel
          isSelected={imgSelected == 0 ? true : false}
          onClick={() => {
            setImgSelected(0);
          }}
        />
        <NavballCarousel
          isSelected={imgSelected == 1 ? true : false}
          onClick={() => {
            setImgSelected(1);
          }}
        />
        <NavballCarousel
          isSelected={imgSelected == 2 ? true : false}
          onClick={() => {
            setImgSelected(2);
          }}
        />
      </ol>
    </div>
  );
}
