import colors from "@/libs/colors";
import Button from "@/components/Button";
import { useVisibility } from "@/libs/useVisibility";
import { useGlobalRefs } from "@/contexts/GlobalRefsContext";

export default function TalentPool({ isMobile }: { isMobile: boolean }) {
  const { talentsGlobalRef } = useGlobalRefs();
  const hasContainerShowed = useVisibility(talentsGlobalRef);
  return (
    <section
      ref={talentsGlobalRef}
      style={{
        visibility: hasContainerShowed ? "visible" : "hidden",
        marginTop: hasContainerShowed ? 100 : 0,
        opacity: hasContainerShowed ? 1 : 0,
        transition:
          "visibility 1s ease-in-out, opacity 1s ease-in-out, margin-top 1s ease-in-out",
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        justifyContent: "space-between",
        alignItems: "center",
        maxWidth: !isMobile ? 1140 : "100%",
        height: 500,
        paddingLeft: 15,
        paddingRight: 15,
      }}
    >
      <div style={{ width: !isMobile ? "60%" : "90vw" }}>
        <img
          style={{ width: isMobile ? "90vw" : undefined }}
          src="https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-84.png"
          alt="Foto de pessoas convidando para o banco de talentos"
        />
      </div>
      <div style={{ width: !isMobile ? "40%" : "100%" }}>
        <h3
          style={{
            color: colors.primary_two,
            fontSize: !isMobile ? 48 : 32,
            fontStyle: "normal",
            fontWeight: 500,
            marginTop: 15,
          }}
        >
          Não encontrou a vaga que estava procurando?
        </h3>
        <div
          style={{
            color: colors.black,
            fontSize: 16,
            fontStyle: "normal",
            fontWeight: "normal",
          }}
        >
          <p style={{ marginTop: 15 }}>
            Não encontrou uma vaga aberta com o seu perfil?
          </p>
          <p style={{ marginTop: 30 }}>
            Não se preocupe! Cadastre-se em nosso Banco de Talentos e fique por
            dentro de todas as oportunidades da Arco. :)
          </p>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "100%",
            }}
          >
            <Button
              width={!isMobile ? 410 : 380}
              height={!isMobile ? 80 : 60}
              onClick={() => {
                alert(true);
              }}
              shadowButton={true}
              shadowLeft={!isMobile ? 20 : 15}
              shadowBottom={!isMobile ? 70 : 55}
              style={{
                marginTop: 30,
                fontSize: 28,
              }}
            >
              Enviar Currículo
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
}
