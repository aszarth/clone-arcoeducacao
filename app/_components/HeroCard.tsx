import colors from "@/libs/colors";

export default function HeroCard() {
  return (
    <section
      style={{
        backgroundImage: `url("https://arcoeducacao.com.br/wp-content/uploads/2022/02/Group-192-2.jpg")`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: "100vw",
        minHeight: 595,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundPositionX: "center",
      }}
    >
      <h2
        style={{
          color: colors.white,
          fontSize: 45,
          textAlign: "center",
          width: 700,
          fontStyle: "normal",
          fontWeight: 500,
        }}
      >
        Educação para construir o futuro. Tecnologia para ir além.
      </h2>
    </section>
  );
}
