import React, {
  createContext,
  useContext,
  useRef,
  ReactNode,
  RefObject,
} from "react";

interface GlobalRefs {
  jobsGlobalRef: RefObject<HTMLDivElement>;
  aboutusGlobalRef: RefObject<HTMLDivElement>;
  ourwayGlobalRef: RefObject<HTMLDivElement>;
  talentsGlobalRef: RefObject<HTMLDivElement>;
}

const GlobalRefsContext = createContext<GlobalRefs | null>(null);

export const GlobalRefsProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const jobsGlobalRef = useRef<HTMLDivElement>(null);
  const aboutusGlobalRef = useRef<HTMLDivElement>(null);
  const ourwayGlobalRef = useRef<HTMLDivElement>(null);
  const talentsGlobalRef = useRef<HTMLDivElement>(null);

  const globalRefs: GlobalRefs = {
    jobsGlobalRef,
    aboutusGlobalRef,
    ourwayGlobalRef,
    talentsGlobalRef,
  };

  return (
    <GlobalRefsContext.Provider value={globalRefs}>
      {children}
    </GlobalRefsContext.Provider>
  );
};

export const useGlobalRefs = (): GlobalRefs => {
  const context = useContext(GlobalRefsContext);
  if (!context) {
    throw new Error("useGlobalRefs must be used within a GlobalRefsProvider");
  }
  return context;
};

export default GlobalRefsContext;
