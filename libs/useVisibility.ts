import { useEffect, useState, RefObject } from "react";

export function useVisibility(elementRef: RefObject<HTMLElement>): boolean {
  const [isVisible, setIsVisible] = useState(false);
  const threshold = 0.1;

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            setIsVisible(true);
            observer.disconnect(); // Desconecta o observador após a primeira visibilidade
          }
        });
      },
      { threshold }
    );

    if (elementRef.current) {
      observer.observe(elementRef.current);
    }

    return () => {
      observer.disconnect();
    };
  }, [elementRef, threshold]);

  return isVisible;
}
