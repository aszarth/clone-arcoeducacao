const colors = {
  primary: "#161B33",
  primary_two: "#777BF7",
  secondary: "#E55D28",
  white: "#fff",
  black: "#161B33",
  orange_call_action: "#E8442E",
};

export default colors;
